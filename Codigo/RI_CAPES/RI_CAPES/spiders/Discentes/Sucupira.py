# -*- coding: utf-8 -*-
import scrapy
import re
import sys

def AcertaTexto(texto):
    novo_texto = texto

    novo_texto = novo_texto.replace('&Ccedil;', 'Ç')
    novo_texto = novo_texto.replace('&ccedil;', 'ç')

    novo_texto = novo_texto.replace('&Atilde;', 'Ã')
    novo_texto = novo_texto.replace('&atilde;', 'ã')
    novo_texto = novo_texto.replace('&Otilde;', 'Õ')
    novo_texto = novo_texto.replace('&otilde;', 'õ')

    novo_texto = novo_texto.replace('&Aacute;', 'Á')
    novo_texto = novo_texto.replace('&aacute;', 'á')
    novo_texto = novo_texto.replace('&Eacute;', 'É')
    novo_texto = novo_texto.replace('&eacute;', 'é')
    novo_texto = novo_texto.replace('&Iacute;', 'Í')
    novo_texto = novo_texto.replace('&iacute;', 'í')
    novo_texto = novo_texto.replace('&Oacute;', 'Ó')
    novo_texto = novo_texto.replace('&oacute;', 'ó')
    novo_texto = novo_texto.replace('&Uacute;', 'Ú')
    novo_texto = novo_texto.replace('&uacute;', 'ú')

    novo_texto = novo_texto.replace('&Acirc;', 'Â')
    novo_texto = novo_texto.replace('&acirc;', 'â')
    novo_texto = novo_texto.replace('&Ecirc;', 'Ê')
    novo_texto = novo_texto.replace('&ecirc;', 'ê')

    novo_texto = novo_texto.replace('&#8217;', '\'')

    novo_texto = novo_texto.replace('&Agrave;', 'À')
    novo_texto = novo_texto.replace('&agrave;', 'à')

    return novo_texto

class SucupiraSpider(scrapy.Spider):
    name = 'Sucupira'
    allowed_domains = ['sucupira.capes.gov.br']

    file0 = open("params.txt", "r")
    parametros = file0.readline()
    dados = re.search('(.*)//(.*)//(.*)//', parametros)
    jsessionid = str(dados.group(1))
    viewstate = str(dados.group(2))
    iD = str(dados.group(3))

    parametros = file0.readline()
    dados = re.search('(.*)//(.*)//(.*)//(.*)//(.*)', parametros)
    IDInstituicao = str(dados.group(1))
    NomeInstituicao = str(dados.group(2))
    IDCurso = str(dados.group(3))
    IDCalendario = str(dados.group(5))

    start_urls = [
        'https://sucupira.capes.gov.br/sucupira/public/consultas/coleta/envioColeta/dadosFotoEnvioColeta.jsf;jsessionid=' + str(jsessionid),
    ]
    def parse(self, response):
        yield scrapy.FormRequest(
            'https://sucupira.capes.gov.br/sucupira/public/consultas/coleta/envioColeta/dadosFotoEnvioColeta.jsf;jsessionid=' + str(self.jsessionid),
            method='POST',
            formdata={
                'form': 'form',
                'form:j_idt30:calendarioid': str(self.IDCalendario),
                'form:j_idt30:inst:valueId': str(self.IDInstituicao),
                'form:j_idt30:inst:input': str(self.NomeInstituicao),
                'form:j_idt30:j_idt50:': str(self.IDCurso),
                'javax.faces.ViewState': str(self.viewstate),
                'javax.faces.source': 'form:j_idt96:j_idt268:j_idt270',
                'javax.faces.partial.event': 'click',
                'javax.faces.partial.execute': 'form:j_idt96:j_idt268:j_idt270 @component',
                'javax.faces.partial.render':'@component',
                'javax.faces.behavior.event': 'action',
                'org.richfaces.ajax.component': 'form:j_idt96:j_idt268:j_idt270',
                'AJAX:EVENTS_COUNT': '1',
                'javax.faces.partial.ajax': 'true'
            },
            headers={
                'Content-type': 'application/x-www-form-urlencoded;charset=UTF-8',
                'Faces-Request': 'partial/ajax',
                'Referer': 'https://sucupira.capes.gov.br/sucupira/public/consultas/coleta/envioColeta/dadosFotoEnvioColeta.jsf',
                'Sec-Fetch-Mode': 'cors'
            },
            callback=self.parse_producoes_intelectuais_prof,
        )
    def parse_producoes_intelectuais_prof(self, response):
        #print(str(response.body))
        texto = str(response.body)

        i = 1

        #  A funcao split divide/separa todos professores.
        texto = texto.split('<td>')

        while(i < len(texto)):
        #A cada 3 partes temos: 1a Parte - Nome do discente     2a Parte - Nível    3a Parte - Situação
            aluno = texto[i]
            nivel = texto[i+1]
            situacao = texto[i+2]

            # na 3a parte está o idDiscente  que corresponde ao ID de cada discente para abrir a pagina com detalhes do mesmo
            posIDDiscente = situacao.find('idDiscente=')
            idDiscente = situacao[posIDDiscente+11:posIDDiscente+19]

            aluno = aluno[0:aluno.index('</td>')]
            nivel = nivel[0:nivel.index('</td>')]
            situacao = situacao[0:situacao.index('</td>')]
            #print('nome ', aluno, ' nivel ', nivel, 'situacao ', situacao, 'iddiscente', idDiscente)
            # Passa para o proximo aluno
            i = i + 3
            if situacao == 'TITULADO':
                urlPagina = 'https://sucupira.capes.gov.br/sucupira/public/consultas/coleta/envioColeta/detalhesDados/viewDiscente.jsf?popup=true&idDiscente=' + idDiscente
                yield response.follow(urlPagina, self.parse_discentes)

                #imprime = '{\n\"Nome\": ' + '\"' + aluno + '\"' + ',\n\"Nivel\": '  '\"' + nivel + '\"\n},\n'
                #if i < len(texto):
                    #    imprime = imprime + ','


    def parse_discentes(self, response):
        texto = str(response.body)
        #print(texto)

        # Extraindo o nome do do discente
        posNome = texto.find('Nome:')
        posSexo = texto.find('Sexo:')
        aluno = texto[posNome:posSexo]
        aluno = aluno[aluno.index('<div class="col-md-10">')+23: aluno.index('<div class="row">')-68]

        #Data da Matricula
        posMat = texto.find('Data da Matr&iacute;cula:')
        posSit = texto.find('Situa & ccedil; & atilde;o:')
        DtMatricula = texto[posMat:posSit]
        DtMatricula = DtMatricula[DtMatricula.index('<div class="col-md-10">') + 23: DtMatricula.index('<div class="col-md-10">') + 33]

        #Data da Situacao
        posSit = texto.find('Data da Situa&ccedil;&atilde;o:')
        posPossuiBolsa = texto.find('Possui autoriza&ccedil;&atilde;o para complementa&ccedil;&atilde;o de bolsa:')
        DtSituacao = texto[posSit:posPossuiBolsa]
        DtSituacao = DtSituacao[DtSituacao.index('<div class="col-md-10">') + 23: DtSituacao.index('<div class="col-md-10">') + 33]

        if (str(self.IDCalendario) == '579'):
            Ano = '2018'
        else:
            Ano = '2017'

        # Para encontrar os autores
        posOri = texto.find('Orienta&ccedil;&otilde;es')
        posBolsas= texto.find('Bolsas')
        lista_orientadores = texto[posOri:posBolsas]
        #  A funcao split divide/separa todos autores.
        lista_orientadores = lista_orientadores.split('<td>')

        imprime = '{\n\"Aluno\": ' + '\"' + aluno + '\"' + ',\n\"DataMatricula\": '  '\"' + DtMatricula + '\"'',\n\"DataSituacao\": ' + '\"' + DtSituacao + '\",\n\"Ano\":"' + Ano + '\",'
        imprime = imprime + '\n\"Orientadores\": ['

        #Para incluir os orientadores
        i = 1
        while (i < len(lista_orientadores)):
            #A cada 3 partes temos: 1a Parte - Nome do orientador     2a Parte - Período de Orientação    3a Parte - Principal (Sim ou Não)
            orientador = lista_orientadores[i]
            # Copia até o proximo </td>
            orientador = orientador[0:orientador.index('</td>')]

            periodo = lista_orientadores[i + 1]
            # Copia até o proximo </td>
            periodo = periodo[0:10] + ' a ' + periodo[40:50]

            principal = lista_orientadores[i + 2]
            principal = principal[0:3]
            imprime = imprime + '\n{\n\"Orientador\":\"' + orientador + '\",\n\"Periodo\":\"' + periodo + '\",\n\"Principal\":\"' + principal + '\"\n}'

            i = i + 3
            if i < len(lista_orientadores):
                imprime = imprime + ','

        imprime = imprime + '\n]\n},\n'

        imprime = AcertaTexto(imprime)
        file_discentes = open("discentes.txt", "a")
        file_discentes.write(imprime)
        #file_discentes.write(']\n}\n]')
        file_discentes.close()