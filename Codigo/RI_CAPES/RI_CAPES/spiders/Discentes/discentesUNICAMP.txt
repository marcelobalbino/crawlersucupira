{
"Aluno": "ANDRE CARVALHO SILVA",
"DataMatricula": "05/06/2013",
"DataSituacao": "16/07/2018",
"Ano":"2018",
"Orientadores": [
{
"Orientador":"ORLANDO LEE",
"Periodo":"05/06/2013 a 16/07/2018",
"Principal":"Sim"
}
]
},
{
"Aluno": "BRUNO DA SILVA MELO",
"DataMatricula": "01/03/2015",
"DataSituacao": "25/06/2018",
"Ano":"2018",
"Orientadores": [
{
"Orientador":"PAULO LICIO DE GEUS",
"Periodo":"01/03/2015 a 25/06/2018",
"Principal":"Sim"
}
]
},
{
"Aluno": "ARISTIDES DE ALMEIDA NETO",
"DataMatricula": "17/02/2016",
"DataSituacao": "26/03/2018",
"Ano":"2018",
"Orientadores": [
{
"Orientador":"ELIANE MARTINS",
"Periodo":"17/02/2016 a 26/03/2018",
"Principal":"Sim"
}
]
},
{
"Aluno": "ATILIO GOMES LUIZ",
"DataMatricula": "29/04/2014",
"DataSituacao": "21/05/2018",
"Ano":"2018",
"Orientadores": [
{
"Orientador":"CHRISTIANE NEME CAMPOS",
"Periodo":"29/04/2014 a 21/05/2018",
"Principal":"Sim"
}
]
},
{
"Aluno": "ALLAN DA SILVA PINTO",
"DataMatricula": "19/03/2014",
"DataSituacao": "06/09/2018",
"Ano":"2018",
"Orientadores": [
{
"Orientador":"ANDERSON DE REZENDE ROCHA",
"Periodo":"19/03/2014 a 06/09/2018",
"Principal":"Sim"
},
{
"Orientador":"HELIO PEDRINI",
"Periodo":"19/03/2014 a 06/09/2018",
"Principal":"N�o"
}
]
},
{
"Aluno": "ADAN ECHEMENDIA MONTERO",
"DataMatricula": "01/09/2015",
"DataSituacao": "25/05/2018",
"Ano":"2018",
"Orientadores": [
{
"Orientador":"ALEXANDRE XAVIER FALCAO",
"Periodo":"01/09/2015 a 25/05/2018",
"Principal":"Sim"
}
]
},
{
"Aluno": "DANIEL GUIMARAES DO LAGO",
"DataMatricula": "01/03/2010",
"DataSituacao": "28/02/2018",
"Ano":"2018",
"Orientadores": [
{
"Orientador":"EDMUNDO ROBERTO MAURO MADEIRA",
"Periodo":"01/01/2012 a 28/02/2018",
"Principal":"Sim"
}
]
},
{
"Aluno": "EDER MAICOL GOMEZ ZEGARRA",
"DataMatricula": "01/01/2016",
"DataSituacao": "24/04/2018",
"Ano":"2018",
"Orientadores": [
{
"Orientador":"GUIDO COSTA SOUZA DE ARAUJO",
"Periodo":"01/01/2016 a 24/04/2018",
"Principal":"Sim"
}
]
},
{
"Aluno": "DIEGO OLIVEIRA RODRIGUES",
"DataMatricula": "01/01/2016",
"DataSituacao": "23/03/2018",
"Ano":"2018",
"Orientadores": [
{
"Orientador":"LEANDRO APARECIDO VILLAS",
"Periodo":"01/01/2016 a 23/03/2018",
"Principal":"Sim"
}
]
},
{
"Aluno": "CELSO AIMBIRE WEFFORT SANTOS",
"DataMatricula": "17/02/2016",
"DataSituacao": "22/05/2018",
"Ano":"2018",
"Orientadores": [
{
"Orientador":"CHRISTIANE NEME CAMPOS",
"Periodo":"17/02/2016 a 22/05/2018",
"Principal":"Sim"
}
]
},
{
"Aluno": "CIRO LUIZ ARAUJO CEISSLER",
"DataMatricula": "15/02/2017",
"DataSituacao": "17/10/2018",
"Ano":"2018",
"Orientadores": [
{
"Orientador":"GUIDO COSTA SOUZA DE ARAUJO",
"Periodo":"15/02/2017 a 17/10/2018",
"Principal":"Sim"
}
]
},
{
"Aluno": "CAMILLA VALERIA DE LIMA TENORIO",
"DataMatricula": "01/08/2016",
"DataSituacao": "13/12/2018",
"Ano":"2018",
"Orientadores": [
{
"Orientador":"MARIA CECILIA CALANI BARANAUSKAS",
"Periodo":"02/04/2018 a 13/12/2018",
"Principal":"Sim"
},
{
"Orientador":"HEIKO HORST HORNUNG",
"Periodo":"01/08/2016 a 02/04/2018",
"Principal":"N�o"
}
]
},
{
"Aluno": "DIOGO MACHADO GONCALVES",
"DataMatricula": "17/02/2016",
"DataSituacao": "10/10/2018",
"Ano":"2018",
"Orientadores": [
{
"Orientador":"EDMUNDO ROBERTO MAURO MADEIRA",
"Periodo":"08/06/2016 a 10/10/2018",
"Principal":"Sim"
},
{
"Orientador":"LUIZ FERNANDO BITTENCOURT",
"Periodo":"01/08/2016 a 10/10/2018",
"Principal":"N�o"
}
]
},
{
"Aluno": "FRANCISCO JHONATAS MELO DA SILVA",
"DataMatricula": "01/08/2016",
"DataSituacao": "24/08/2018",
"Ano":"2018",
"Orientadores": [
{
"Orientador":"FLAVIO KEIDI MIYAZAWA",
"Periodo":"19/08/2016 a 24/08/2018",
"Principal":"Sim"
},
{
"Orientador":"RAFAEL CRIVELLARI SALIBA SCHOUERY",
"Periodo":"11/11/2016 a 24/08/2018",
"Principal":"N�o"
}
]
},
{
"Aluno": "GUILHERME CANO LOPES",
"DataMatricula": "01/08/2016",
"DataSituacao": "23/10/2018",
"Ano":"2018",
"Orientadores": [
{
"Orientador":"ESTHER LUNA COLOMBINI",
"Periodo":"02/08/2016 a 23/10/2018",
"Principal":"Sim"
}
]
},
{
"Aluno": "HAYATO FUJII",
"DataMatricula": "03/08/2015",
"DataSituacao": "22/05/2018",
"Ano":"2018",
"Orientadores": [
{
"Orientador":"DIEGO DE FREITAS ARANHA",
"Periodo":"03/08/2015 a 22/05/2018",
"Principal":"Sim"
}
]
},
{
"Aluno": "JHOSIMAR GEORGE ARIAS FIGUEROA",
"DataMatricula": "01/03/2015",
"DataSituacao": "19/02/2018",
"Ano":"2018",
"Orientadores": [
{
"Orientador":"GERBERTH ADIN RAMIREZ RIVERA",
"Periodo":"19/12/2016 a 19/02/2018",
"Principal":"Sim"
}
]
},
{
"Aluno": "GREICE CRISTINA MARIANO",
"DataMatricula": "09/08/2013",
"DataSituacao": "08/06/2018",
"Ano":"2018",
"Orientadores": [
{
"Orientador":"RICARDO DA SILVA TORRES",
"Periodo":"09/08/2013 a 08/06/2018",
"Principal":"Sim"
},
{
"Orientador":"LEONOR PATRICIA CERDEIRA MORELLATO",
"Periodo":"09/08/2013 a 08/06/2018",
"Principal":"N�o"
}
]
},
{
"Aluno": "HELDER MAY NUNES DA SILVA OLIVEIRA",
"DataMatricula": "19/03/2014",
"DataSituacao": "12/09/2018",
"Ano":"2018",
"Orientadores": [
{
"Orientador":"NELSON LUIS SALDANHA DA FONSECA",
"Periodo":"19/03/2014 a 12/09/2018",
"Principal":"Sim"
}
]
},
{
"Aluno": "JOAO DO MONTE GOMES DUARTE",
"DataMatricula": "17/03/2014",
"DataSituacao": "19/01/2018",
"Ano":"2018",
"Orientadores": [
{
"Orientador":"LEANDRO APARECIDO VILLAS",
"Periodo":"17/03/2014 a 19/01/2018",
"Principal":"Sim"
}
]
},
{
"Aluno": "GUILHERME COLUCCI PEREIRA",
"DataMatricula": "17/02/2016",
"DataSituacao": "28/02/2018",
"Ano":"2018",
"Orientadores": [
{
"Orientador":"HEIKO HORST HORNUNG",
"Periodo":"17/02/2016 a 28/02/2018",
"Principal":"Sim"
}
]
},
{
"Aluno": "GUILHERME ADRIANO FOLEGO",
"DataMatricula": "01/03/2015",
"DataSituacao": "18/12/2018",
"Ano":"2018",
"Orientadores": [
{
"Orientador":"ANDERSON DE REZENDE ROCHA",
"Periodo":"01/03/2015 a 18/12/2018",
"Principal":"Sim"
},
{
"Orientador":"MARINA WEILER",
"Periodo":"15/01/2018 a 18/12/2018",
"Principal":"N�o"
}
]
},
{
"Aluno": "LUAN CARDOSO DOS SANTOS",
"DataMatricula": "01/03/2015",
"DataSituacao": "12/06/2018",
"Ano":"2018",
"Orientadores": [
{
"Orientador":"JULIO CESAR LOPEZ HERNANDEZ",
"Periodo":"01/03/2015 a 12/06/2018",
"Principal":"Sim"
}
]
},
{
"Aluno": "JOSE RAMON TRINDADE PIRES",
"DataMatricula": "01/08/2013",
"DataSituacao": "20/12/2018",
"Ano":"2018",
"Orientadores": [
{
"Orientador":"ANDERSON DE REZENDE ROCHA",
"Periodo":"01/08/2013 a 20/12/2018",
"Principal":"Sim"
}
]
},
{
"Aluno": "JUAN SEBASTIAN BELENO DIAZ",
"DataMatricula": "01/01/2016",
"DataSituacao": "03/05/2018",
"Ano":"2018",
"Orientadores": [
{
"Orientador":"CLAUDIA MARIA BAUZER MEDEIROS",
"Periodo":"01/01/2016 a 03/05/2018",
"Principal":"Sim"
}
]
},
{
"Aluno": "LAIS VASCONCELLOS MINCHILLO",
"DataMatricula": "03/08/2015",
"DataSituacao": "13/06/2018",
"Ano":"2018",
"Orientadores": [
{
"Orientador":"EDSON BORIN",
"Periodo":"03/08/2015 a 13/06/2018",
"Principal":"N�o"
},
{
"Orientador":"JULIANA FREITAG",
"Periodo":"03/08/2015 a 13/06/2018",
"Principal":"Sim"
}
]
},
{
"Aluno": "KLEBER ANDRADE OLIVEIRA",
"DataMatricula": "01/08/2016",
"DataSituacao": "06/08/2018",
"Ano":"2018",
"Orientadores": [
{
"Orientador":"ANDRE SANTANCHE",
"Periodo":"01/08/2016 a 06/08/2018",
"Principal":"Sim"
}
]
},
{
"Aluno": "KLAIRTON DE LIMA BRITO",
"DataMatricula": "17/02/2016",
"DataSituacao": "12/04/2018",
"Ano":"2018",
"Orientadores": [
{
"Orientador":"ZANONI DIAS",
"Periodo":"17/02/2016 a 12/04/2018",
"Principal":"Sim"
}
]
},
{
"Aluno": "LUCAS AUGUSTO MONTALVAO COSTA CARVALHO",
"DataMatricula": "01/09/2014",
"DataSituacao": "14/12/2018",
"Ano":"2018",
"Orientadores": [
{
"Orientador":"CLAUDIA MARIA BAUZER MEDEIROS",
"Periodo":"01/09/2014 a 14/12/2018",
"Principal":"Sim"
},
{
"Orientador":"Yolanda Gil",
"Periodo":"10/07/2017 a 14/12/2018",
"Principal":"N�o"
}
]
},
{
"Aluno": "KENT EMERSHON YUCRA QUISPE",
"DataMatricula": "01/03/2016",
"DataSituacao": "20/04/2018",
"Ano":"2018",
"Orientadores": [
{
"Orientador":"EDUARDO CANDIDO XAVIER",
"Periodo":"01/03/2016 a 20/04/2018",
"Principal":"Sim"
},
{
"Orientador":"CARLA NEGRI LINTZMAYER",
"Periodo":"23/05/2017 a 20/04/2018",
"Principal":"N�o"
}
]
},
{
"Aluno": "LUIZ CLAUDIO NAVARRO",
"DataMatricula": "14/09/2017",
"DataSituacao": "20/04/2018",
"Ano":"2018",
"Orientadores": [
{
"Orientador":"ANDERSON DE REZENDE ROCHA",
"Periodo":"14/09/2017 a 20/04/2018",
"Principal":"N�o"
},
{
"Orientador":"RICARDO DAHAB",
"Periodo":"14/09/2017 a 20/04/2018",
"Principal":"Sim"
}
]
},
{
"Aluno": "LUIS EDUARDO THIBES FORQUESATO",
"DataMatricula": "01/03/2017",
"DataSituacao": "20/12/2018",
"Ano":"2018",
"Orientadores": [
{
"Orientador":"JULIANA FREITAG",
"Periodo":"01/03/2017 a 20/12/2018",
"Principal":"Sim"
}
]
},
{
"Aluno": "LUCIANA BULGARELLI CARVALHO",
"DataMatricula": "01/10/2018",
"DataSituacao": "18/12/2018",
"Ano":"2018",
"Orientadores": [
{
"Orientador":"GUIDO COSTA SOUZA DE ARAUJO",
"Periodo":"01/10/2018 a 18/12/2018",
"Principal":"Sim"
}
]
},
{
"Aluno": "MAURICIO JOSE DE OLIVEIRA ZAMBON",
"DataMatricula": "27/11/2014",
"DataSituacao": "01/10/2018",
"Ano":"2018",
"Orientadores": [
{
"Orientador":"CID CARVALHO DE SOUZA",
"Periodo":"11/01/2015 a 01/10/2018",
"Principal":"N�o"
},
{
"Orientador":"PEDRO JUSSIEU DE REZENDE",
"Periodo":"27/11/2014 a 01/10/2018",
"Principal":"Sim"
}
]
},
{
"Aluno": "LUCIANA BARBIERI",
"DataMatricula": "03/08/2015",
"DataSituacao": "19/01/2018",
"Ano":"2018",
"Orientadores": [
{
"Orientador":"HELIO PEDRINI",
"Periodo":"03/08/2015 a 19/01/2018",
"Principal":"Sim"
}
]
},
{
"Aluno": "LUIS AUGUSTO MARTINS PEREIRA",
"DataMatricula": "11/03/2013",
"DataSituacao": "27/07/2018",
"Ano":"2018",
"Orientadores": [
{
"Orientador":"RICARDO DA SILVA TORRES",
"Periodo":"11/03/2014 a 27/07/2018",
"Principal":"Sim"
}
]
},
{
"Aluno": "MARCOS ROBERTO E SOUZA",
"DataMatricula": "17/02/2016",
"DataSituacao": "23/02/2018",
"Ano":"2018",
"Orientadores": [
{
"Orientador":"HELIO PEDRINI",
"Periodo":"17/02/2016 a 23/02/2018",
"Principal":"Sim"
}
]
},
{
"Aluno": "MARCELO PINHEIRO LEITE BENEDITO",
"DataMatricula": "17/02/2016",
"DataSituacao": "31/07/2018",
"Ano":"2018",
"Orientadores": [
{
"Orientador":"LEHILTON LELIS CHAVES PEDROSA",
"Periodo":"17/02/2016 a 31/07/2018",
"Principal":"Sim"
}
]
},
{
"Aluno": "LUIS FELIPE SOUZA DE MATTOS",
"DataMatricula": "17/02/2016",
"DataSituacao": "16/05/2018",
"Ano":"2018",
"Orientadores": [
{
"Orientador":"GUIDO COSTA SOUZA DE ARAUJO",
"Periodo":"17/02/2016 a 16/05/2018",
"Principal":"Sim"
},
{
"Orientador":"MARCIO MACHADO PEREIRA",
"Periodo":"15/01/2018 a 16/05/2018",
"Principal":"N�o"
}
]
},
{
"Aluno": "NIELSEN CASSIANO SIMOES",
"DataMatricula": "01/03/2003",
"DataSituacao": "15/08/2018",
"Ano":"2018",
"Orientadores": [
{
"Orientador":"ANDRE SANTANCHE",
"Periodo":"23/03/2018 a 15/08/2018",
"Principal":"Sim"
}
]
},
{
"Aluno": "RAI CAETANO DE JESUS",
"DataMatricula": "01/01/2016",
"DataSituacao": "30/01/2018",
"Ano":"2018",
"Orientadores": [
{
"Orientador":"FABIO LUIZ USBERTI",
"Periodo":"01/01/2016 a 30/01/2018",
"Principal":"Sim"
}
]
},
{
"Aluno": "PAULO HENRIQUE HACK DE JESUS",
"DataMatricula": "21/06/2018",
"DataSituacao": "15/10/2018",
"Ano":"2018",
"Orientadores": [
{
"Orientador":"HELIO PEDRINI",
"Periodo":"21/06/2018 a 15/10/2018",
"Principal":"Sim"
}
]
},
{
"Aluno": "NARCISIO JOSE MULA",
"DataMatricula": "16/05/2013",
"DataSituacao": "31/08/2018",
"Ano":"2018",
"Orientadores": [
{
"Orientador":"ELIANE MARTINS",
"Periodo":"01/07/2013 a 31/08/2018",
"Principal":"Sim"
}
]
},
{
"Aluno": "RAFAEL MEDEIROS JACOMEL DE OLIVEIRA SILVA",
"DataMatricula": "15/02/2017",
"DataSituacao": "05/04/2018",
"Ano":"2018",
"Orientadores": [
{
"Orientador":"JACQUES WAINER",
"Periodo":"15/02/2017 a 05/04/2018",
"Principal":"Sim"
}
]
},
{
"Aluno": "PEDRO RIBEIRO MENDES JUNIOR",
"DataMatricula": "01/09/2014",
"DataSituacao": "14/09/2018",
"Ano":"2018",
"Orientadores": [
{
"Orientador":"ANDERSON DE REZENDE ROCHA",
"Periodo":"01/09/2014 a 14/09/2018",
"Principal":"Sim"
}
]
},
{
"Aluno": "MAYCON SAMBINELLI",
"DataMatricula": "28/05/2014",
"DataSituacao": "24/04/2018",
"Ano":"2018",
"Orientadores": [
{
"Orientador":"ORLANDO LEE",
"Periodo":"28/05/2014 a 24/04/2018",
"Principal":"Sim"
}
]
},
{
"Aluno": "NATANAEL RAMOS",
"DataMatricula": "01/01/2016",
"DataSituacao": "13/04/2018",
"Ano":"2018",
"Orientadores": [
{
"Orientador":"CID CARVALHO DE SOUZA",
"Periodo":"01/01/2016 a 13/04/2018",
"Principal":"Sim"
}
]
},
{
"Aluno": "MURILO SANTOS DE LIMA",
"DataMatricula": "11/03/2014",
"DataSituacao": "11/05/2018",
"Ano":"2018",
"Orientadores": [
{
"Orientador":"ORLANDO LEE",
"Periodo":"11/03/2014 a 11/05/2018",
"Principal":"Sim"
},
{
"Orientador":"MARIO CESAR SAN FELICE",
"Periodo":"19/10/2017 a 11/05/2018",
"Principal":"N�o"
}
]
},
{
"Aluno": "RENAN MONTEIRO PINTO NETO",
"DataMatricula": "26/10/2018",
"DataSituacao": "20/12/2018",
"Ano":"2018",
"Orientadores": [
{
"Orientador":"JULIANA FREITAG",
"Periodo":"26/10/2018 a 20/12/2018",
"Principal":"Sim"
}
]
},
{
"Aluno": "ULYSSES ALESSANDRO COUTO ROCHA",
"DataMatricula": "01/08/2016",
"DataSituacao": "10/12/2018",
"Ano":"2018",
"Orientadores": [
{
"Orientador":"FLAVIO KEIDI MIYAZAWA",
"Periodo":"01/08/2016 a 10/12/2018",
"Principal":"Sim"
}
]
},
{
"Aluno": "VANESSA REGINA MARGARETH LIMA MAIKE",
"DataMatricula": "02/12/2013",
"DataSituacao": "25/09/2018",
"Ano":"2018",
"Orientadores": [
{
"Orientador":"MARIA CECILIA CALANI BARANAUSKAS",
"Periodo":"02/12/2013 a 25/09/2018",
"Principal":"Sim"
}
]
},
{
"Aluno": "WALDIR RODRIGUES DE ALMEIDA",
"DataMatricula": "03/08/2015",
"DataSituacao": "25/01/2018",
"Ano":"2018",
"Orientadores": [
{
"Orientador":"ANDERSON DE REZENDE ROCHA",
"Periodo":"03/08/2015 a 25/01/2018",
"Principal":"Sim"
},
{
"Orientador":"FERNANDA ALCANTARA ANDALO",
"Periodo":"13/04/2016 a 25/01/2018",
"Principal":"N�o"
}
]
},
{
"Aluno": "THIERRY PINHEIRO MOREIRA",
"DataMatricula": "26/04/2014",
"DataSituacao": "07/06/2018",
"Ano":"2018",
"Orientadores": [
{
"Orientador":"HELIO PEDRINI",
"Periodo":"26/04/2014 a 07/06/2018",
"Principal":"Sim"
}
]
},
{
"Aluno": "ROBERTO ALEJANDRO HIDALGO CASTRO",
"DataMatricula": "01/01/2016",
"DataSituacao": "09/05/2018",
"Ano":"2018",
"Orientadores": [
{
"Orientador":"LUCAS FRANCISCO WANNER",
"Periodo":"01/03/2016 a 09/05/2018",
"Principal":"Sim"
}
]
},
{
"Aluno": "ALEXANDRE YUKIO YAMASHITA",
"DataMatricula": "01/03/2015",
"DataSituacao": "28/04/2017",
"Ano":"2017",
"Orientadores": [
{
"Orientador":"ALEXANDRE XAVIER FALCAO",
"Periodo":"21/10/2016 a 28/04/2017",
"Principal":"Sim"
}
]
},
{
"Aluno": "ALEXANDRE MELO BRAGA",
"DataMatricula": "01/08/2012",
"DataSituacao": "19/12/2017",
"Ano":"2017",
"Orientadores": [
{
"Orientador":"RICARDO DAHAB",
"Periodo":"01/08/2012 a 19/12/2017",
"Principal":"Sim"
}
]
},
{
"Aluno": "ALEXANDRE ESTEVES ALMEIDA",
"DataMatricula": "01/03/2015",
"DataSituacao": "04/08/2017",
"Ano":"2017",
"Orientadores": [
{
"Orientador":"RICARDO DA SILVA TORRES",
"Periodo":"01/03/2015 a 04/08/2017",
"Principal":"Sim"
}
]
},
{
"Aluno": "ALAN ZANONI PEIXINHO",
"DataMatricula": "01/03/2014",
"DataSituacao": "05/05/2017",
"Ano":"2017",
"Orientadores": [
{
"Orientador":"ALEXANDRE XAVIER FALCAO",
"Periodo":"18/03/2014 a 05/05/2017",
"Principal":"Sim"
}
]
},
{
"Aluno": "ALEXANDRE TOSHIO HIRATA",
"DataMatricula": "01/09/2014",
"DataSituacao": "15/12/2017",
"Ano":"2017",
"Orientadores": [
{
"Orientador":"JULIANA FREITAG",
"Periodo":"01/09/2014 a 15/12/2017",
"Principal":"Sim"
}
]
},
{
"Aluno": "ALYSSON BOLOGNESI PRADO",
"DataMatricula": "01/03/2011",
"DataSituacao": "07/03/2017",
"Ano":"2017",
"Orientadores": [
{
"Orientador":"MARIA CECILIA CALANI BARANAUSKAS",
"Periodo":"01/01/2012 a 07/03/2017",
"Principal":"Sim"
}
]
},
{
"Aluno": "AZAEL DE MELO E SOUSA",
"DataMatricula": "03/08/2015",
"DataSituacao": "09/11/2017",
"Ano":"2017",
"Orientadores": [
{
"Orientador":"ALEXANDRE XAVIER FALCAO",
"Periodo":"03/08/2015 a 09/11/2017",
"Principal":"Sim"
}
]
},
{
"Aluno": "ANDERSON ROSSANEZ",
"DataMatricula": "03/08/2015",
"DataSituacao": "27/07/2017",
"Ano":"2017",
"Orientadores": [
{
"Orientador":"ARIADNE MARIA BRITO RIZZONI CARVALHO",
"Periodo":"03/08/2015 a 27/07/2017",
"Principal":"Sim"
}
]
},
{
"Aluno": "ANA KARINA DOURADO SALINA DE OLIVEIRA",
"DataMatricula": "01/08/2009",
"DataSituacao": "07/04/2017",
"Ano":"2017",
"Orientadores": [
{
"Orientador":"JULIO CESAR LOPEZ HERNANDEZ",
"Periodo":"01/01/2012 a 07/04/2017",
"Principal":"Sim"
}
]
},
{
"Aluno": "AUGUSTO RODRIGUES DE SOUZA",
"DataMatricula": "25/07/2013",
"DataSituacao": "28/04/2017",
"Ano":"2017",
"Orientadores": [
{
"Orientador":"ISLENE CALCIOLARI GARCIA",
"Periodo":"01/08/2013 a 28/04/2017",
"Principal":"Sim"
}
]
},
{
"Aluno": "ACAUAN CARDOSO RIBEIRO",
"DataMatricula": "01/03/2015",
"DataSituacao": "22/02/2017",
"Ano":"2017",
"Orientadores": [
{
"Orientador":"GUILHERME PIMENTEL TELLES",
"Periodo":"01/03/2015 a 22/02/2017",
"Principal":"Sim"
}
]
},
{
"Aluno": "ELISA DE CASSIA SILVA RODRIGUES",
"DataMatricula": "01/08/2011",
"DataSituacao": "10/02/2017",
"Ano":"2017",
"Orientadores": [
{
"Orientador":"JORGE STOLFI",
"Periodo":"01/01/2012 a 10/02/2017",
"Principal":"Sim"
}
]
},
{
"Aluno": "DANIELE CRISTINA UCHOA MAIA RODRIGUES",
"DataMatricula": "25/02/2013",
"DataSituacao": "29/09/2017",
"Ano":"2017",
"Orientadores": [
{
"Orientador":"RICARDO DA SILVA TORRES",
"Periodo":"25/06/2013 a 29/09/2017",
"Principal":"Sim"
}
]
},
{
"Aluno": "ERIC CARVALHO OAKLEY",
"DataMatricula": "01/03/2015",
"DataSituacao": "13/02/2017",
"Ano":"2017",
"Orientadores": [
{
"Orientador":"RICARDO DA SILVA TORRES",
"Periodo":"01/03/2015 a 13/02/2017",
"Principal":"Sim"
}
]
},
{
"Aluno": "DANIEL RODRIGUES CARVALHO",
"DataMatricula": "03/08/2015",
"DataSituacao": "20/07/2017",
"Ano":"2017",
"Orientadores": [
{
"Orientador":"RODOLFO JARDIM DE AZEVEDO",
"Periodo":"03/08/2015 a 20/07/2017",
"Principal":"Sim"
}
]
},
{
"Aluno": "CRISTIANO BORGES CARDOSO",
"DataMatricula": "25/07/2013",
"DataSituacao": "18/12/2017",
"Ano":"2017",
"Orientadores": [
{
"Orientador":"LEANDRO APARECIDO VILLAS",
"Periodo":"27/07/2013 a 18/12/2017",
"Principal":"Sim"
}
]
},
{
"Aluno": "EDSON ARIEL TICONA ZEGARRA",
"DataMatricula": "01/09/2014",
"DataSituacao": "24/08/2017",
"Ano":"2017",
"Orientadores": [
{
"Orientador":"LEANDRO APARECIDO VILLAS",
"Periodo":"16/12/2016 a 24/08/2017",
"Principal":"N�o"
},
{
"Orientador":"RAFAEL CRIVELLARI SALIBA SCHOUERY",
"Periodo":"07/11/2016 a 24/08/2017",
"Principal":"Sim"
}
]
},
{
"Aluno": "DARWIN DANILO SAIRE PILCO",
"DataMatricula": "01/03/2015",
"DataSituacao": "31/03/2017",
"Ano":"2017",
"Orientadores": [
{
"Orientador":"ALEXANDRE XAVIER FALCAO",
"Periodo":"01/03/2015 a 31/03/2017",
"Principal":"Sim"
}
]
},
{
"Aluno": "DIVINO CESAR SOARES LUCAS",
"DataMatricula": "09/04/2013",
"DataSituacao": "06/10/2017",
"Ano":"2017",
"Orientadores": [
{
"Orientador":"EDSON BORIN",
"Periodo":"09/04/2013 a 06/10/2017",
"Principal":"N�o"
},
{
"Orientador":"GUIDO COSTA SOUZA DE ARAUJO",
"Periodo":"09/04/2013 a 06/10/2017",
"Principal":"Sim"
}
]
},
{
"Aluno": "DANIEL CASON",
"DataMatricula": "01/03/2012",
"DataSituacao": "31/03/2017",
"Ano":"2017",
"Orientadores": [
{
"Orientador":"LUIZ EDUARDO BUZATO",
"Periodo":"01/03/2012 a 31/03/2017",
"Principal":"Sim"
}
]
},
{
"Aluno": "FELIX CARVALHO RODRIGUES",
"DataMatricula": "25/02/2013",
"DataSituacao": "16/10/2017",
"Ano":"2017",
"Orientadores": [
{
"Orientador":"EDUARDO CANDIDO XAVIER",
"Periodo":"01/03/2013 a 16/10/2017",
"Principal":"Sim"
},
{
"Orientador":"FLAVIO KEIDI MIYAZAWA",
"Periodo":"01/03/2013 a 16/10/2017",
"Principal":"N�o"
}
]
},
{
"Aluno": "GIOVANI FRONDANA",
"DataMatricula": "01/03/2015",
"DataSituacao": "16/03/2017",
"Ano":"2017",
"Orientadores": [
{
"Orientador":"JACQUES WAINER",
"Periodo":"01/03/2015 a 16/03/2017",
"Principal":"Sim"
}
]
},
{
"Aluno": "FRANCISCO JOSE NARDI FILHO",
"DataMatricula": "01/03/2015",
"DataSituacao": "19/05/2017",
"Ano":"2017",
"Orientadores": [
{
"Orientador":"ANDRE SANTANCHE",
"Periodo":"06/08/2015 a 19/05/2017",
"Principal":"Sim"
}
]
},
{
"Aluno": "GEISE KELLY DA SILVA SANTOS",
"DataMatricula": "01/09/2014",
"DataSituacao": "21/03/2017",
"Ano":"2017",
"Orientadores": [
{
"Orientador":"ANDERSON DE REZENDE ROCHA",
"Periodo":"01/09/2014 a 21/03/2017",
"Principal":"Sim"
}
]
},
{
"Aluno": "ERICK LUIS MORAES DE SOUSA",
"DataMatricula": "11/03/2014",
"DataSituacao": "11/04/2017",
"Ano":"2017",
"Orientadores": [
{
"Orientador":"HELIO PEDRINI",
"Periodo":"30/04/2014 a 11/04/2017",
"Principal":"Sim"
}
]
},
{
"Aluno": "FELIPE ALVES DA LOUZA",
"DataMatricula": "25/07/2013",
"DataSituacao": "27/07/2017",
"Ano":"2017",
"Orientadores": [
{
"Orientador":"GUILHERME PIMENTEL TELLES",
"Periodo":"01/08/2013 a 27/07/2017",
"Principal":"Sim"
},
{
"Orientador":"Simon Gog",
"Periodo":"17/06/2015 a 27/07/2017",
"Principal":"N�o"
}
]
},
{
"Aluno": "JANE DIRCE ALVES SANDIM ELEUTERIO",
"DataMatricula": "01/08/2010",
"DataSituacao": "28/07/2017",
"Ano":"2017",
"Orientadores": [
{
"Orientador":"CECILIA MARY FISCHER RUBIRA",
"Periodo":"01/01/2012 a 28/07/2017",
"Principal":"Sim"
}
]
},
{
"Aluno": "JEFERSON RECH BRUNETTA",
"DataMatricula": "01/09/2014",
"DataSituacao": "17/04/2017",
"Ano":"2017",
"Orientadores": [
{
"Orientador":"EDSON BORIN",
"Periodo":"01/09/2014 a 17/04/2017",
"Principal":"Sim"
}
]
},
{
"Aluno": "JAUDETE DALTIO",
"DataMatricula": "01/03/2012",
"DataSituacao": "04/09/2017",
"Ano":"2017",
"Orientadores": [
{
"Orientador":"CLAUDIA MARIA BAUZER MEDEIROS",
"Periodo":"01/03/2012 a 04/09/2017",
"Principal":"Sim"
}
]
},
{
"Aluno": "IVELIZE ROCHA BERNARDO",
"DataMatricula": "01/08/2010",
"DataSituacao": "24/10/2017",
"Ano":"2017",
"Orientadores": [
{
"Orientador":"ANDRE SANTANCHE",
"Periodo":"01/01/2012 a 24/10/2017",
"Principal":"Sim"
}
]
},
{
"Aluno": "HILARIO SEIBEL JUNIOR",
"DataMatricula": "25/02/2013",
"DataSituacao": "29/09/2017",
"Ano":"2017",
"Orientadores": [
{
"Orientador":"SIOME KLEIN GOLDENSTEIN",
"Periodo":"10/04/2013 a 29/09/2017",
"Principal":"Sim"
}
]
},
{
"Aluno": "HERCULES CARDOSO DA SILVA",
"DataMatricula": "01/09/2017",
"DataSituacao": "06/11/2017",
"Ano":"2017",
"Orientadores": [
{
"Orientador":"EDSON BORIN",
"Periodo":"01/09/2017 a 06/11/2017",
"Principal":"Sim"
}
]
},
{
"Aluno": "HUGO KOOKI KASUYA ROSADO",
"DataMatricula": "03/08/2015",
"DataSituacao": "02/10/2017",
"Ano":"2017",
"Orientadores": [
{
"Orientador":"FLAVIO KEIDI MIYAZAWA",
"Periodo":"23/05/2017 a 02/10/2017",
"Principal":"N�o"
},
{
"Orientador":"LEHILTON LELIS CHAVES PEDROSA",
"Periodo":"22/05/2017 a 02/10/2017",
"Principal":"Sim"
},
{
"Orientador":"FLAVIO KEIDI MIYAZAWA",
"Periodo":"16/01/2016 a 22/05/2017",
"Principal":"N�o"
}
]
},
{
"Aluno": "LEYDI ROCIO ERAZO PARUMA",
"DataMatricula": "01/09/2014",
"DataSituacao": "11/12/2017",
"Ano":"2017",
"Orientadores": [
{
"Orientador":"ELIANE MARTINS",
"Periodo":"01/04/2015 a 11/12/2017",
"Principal":"Sim"
}
]
},
{
"Aluno": "LEANDRO TACIOLI",
"DataMatricula": "01/03/2015",
"DataSituacao": "03/07/2017",
"Ano":"2017",
"Orientadores": [
{
"Orientador":"CLAUDIA MARIA BAUZER MEDEIROS",
"Periodo":"01/03/2015 a 03/07/2017",
"Principal":"Sim"
}
]
},
{
"Aluno": "LUA MARCELO MURIANA",
"DataMatricula": "01/03/2015",
"DataSituacao": "11/09/2017",
"Ano":"2017",
"Orientadores": [
{
"Orientador":"HEIKO HORST HORNUNG",
"Periodo":"01/03/2015 a 11/09/2017",
"Principal":"Sim"
}
]
},
{
"Aluno": "JULIO CESAR MENDOZA BOBADILLA",
"DataMatricula": "01/03/2016",
"DataSituacao": "18/12/2017",
"Ano":"2017",
"Orientadores": [
{
"Orientador":"HELIO PEDRINI",
"Periodo":"01/03/2016 a 18/12/2017",
"Principal":"Sim"
}
]
},
{
"Aluno": "LEONARA DE MEDEIROS BRAZ",
"DataMatricula": "01/03/2015",
"DataSituacao": "11/12/2017",
"Ano":"2017",
"Orientadores": [
{
"Orientador":"HEIKO HORST HORNUNG",
"Periodo":"27/07/2015 a 11/12/2017",
"Principal":"Sim"
}
]
},
{
"Aluno": "LUCAS CARVALHO LEAL",
"DataMatricula": "01/03/2015",
"DataSituacao": "14/12/2017",
"Ano":"2017",
"Orientadores": [
{
"Orientador":"ELIANE MARTINS",
"Periodo":"14/08/2015 a 14/12/2017",
"Principal":"Sim"
},
{
"Orientador":"Andrea Ceccarelli",
"Periodo":"14/08/2015 a 14/12/2017",
"Principal":"N�o"
}
]
},
{
"Aluno": "JUAN FELIPE HERNANDEZ ALBARRACIN",
"DataMatricula": "01/03/2015",
"DataSituacao": "17/03/2017",
"Ano":"2017",
"Orientadores": [
{
"Orientador":"RICARDO DA SILVA TORRES",
"Periodo":"08/07/2015 a 17/03/2017",
"Principal":"Sim"
}
]
},
{
"Aluno": "LUIS GUILHERME CORDIOLLI RUSSI",
"DataMatricula": "01/03/2012",
"DataSituacao": "14/07/2017",
"Ano":"2017",
"Orientadores": [
{
"Orientador":"EDMUNDO ROBERTO MAURO MADEIRA",
"Periodo":"01/03/2012 a 14/07/2017",
"Principal":"Sim"
}
]
},
{
"Aluno": "MATHEUS SILVA MOTA",
"DataMatricula": "01/03/2012",
"DataSituacao": "18/12/2017",
"Ano":"2017",
"Orientadores": [
{
"Orientador":"ANDRE SANTANCHE",
"Periodo":"01/03/2012 a 18/12/2017",
"Principal":"Sim"
}
]
},
{
"Aluno": "MARCIO MORAES LOPES",
"DataMatricula": "01/03/2015",
"DataSituacao": "12/07/2017",
"Ano":"2017",
"Orientadores": [
{
"Orientador":"LUIZ FERNANDO BITTENCOURT",
"Periodo":"01/03/2015 a 12/07/2017",
"Principal":"Sim"
}
]
},
{
"Aluno": "MARIO MIKIO HATO",
"DataMatricula": "01/08/2011",
"DataSituacao": "03/02/2017",
"Ano":"2017",
"Orientadores": [
{
"Orientador":"EDSON BORIN",
"Periodo":"01/01/2012 a 03/02/2017",
"Principal":"Sim"
},
{
"Orientador":"RODOLFO JARDIM DE AZEVEDO",
"Periodo":"01/08/2011 a 03/02/2017",
"Principal":"N�o"
}
]
},
{
"Aluno": "MARCUS FELIPE BOTACIN",
"DataMatricula": "03/08/2015",
"DataSituacao": "28/07/2017",
"Ano":"2017",
"Orientadores": [
{
"Orientador":"PAULO LICIO DE GEUS",
"Periodo":"03/08/2015 a 28/07/2017",
"Principal":"Sim"
},
{
"Orientador":"ANDRE RICARDO ABED GREGIO",
"Periodo":"01/09/2015 a 28/07/2017",
"Principal":"N�o"
}
]
},
{
"Aluno": "MAURICIO GAGLIARDI PALMA",
"DataMatricula": "03/08/2015",
"DataSituacao": "18/09/2017",
"Ano":"2017",
"Orientadores": [
{
"Orientador":"RODOLFO JARDIM DE AZEVEDO",
"Periodo":"03/08/2015 a 18/09/2017",
"Principal":"Sim"
},
{
"Orientador":"EMILIO DE CAMARGO FRANCESQUINI",
"Periodo":"19/12/2016 a 18/09/2017",
"Principal":"N�o"
}
]
},
{
"Aluno": "SAMUEL BASTOS BUCHDID",
"DataMatricula": "13/09/2017",
"DataSituacao": "13/12/2017",
"Ano":"2017",
"Orientadores": [
{
"Orientador":"MARIA CECILIA CALANI BARANAUSKAS",
"Periodo":"13/09/2017 a 13/12/2017",
"Principal":"Sim"
}
]
},
{
"Aluno": "RAFAEL CARDOSO FERNANDES SOUSA",
"DataMatricula": "01/01/2015",
"DataSituacao": "24/03/2017",
"Ano":"2017",
"Orientadores": [
{
"Orientador":"GUIDO COSTA SOUZA DE ARAUJO",
"Periodo":"01/03/2015 a 24/03/2017",
"Principal":"Sim"
}
]
},
{
"Aluno": "PAULO EDUARDO RAUBER",
"DataMatricula": "01/08/2012",
"DataSituacao": "20/02/2017",
"Ano":"2017",
"Orientadores": [
{
"Orientador":"ALEXANDRE XAVIER FALCAO",
"Periodo":"01/08/2012 a 20/02/2017",
"Principal":"Sim"
},
{
"Orientador":"PEDRO JUSSIEU DE REZENDE",
"Periodo":"01/08/2012 a 20/02/2017",
"Principal":"N�o"
},
{
"Orientador":"Alexandru Cristian Telea",
"Periodo":"01/03/2014 a 20/02/2017",
"Principal":"N�o"
}
]
},
{
"Aluno": "PEDRO PAULO LIBORIO LIMA DO NASCIMENTO",
"DataMatricula": "01/03/2015",
"DataSituacao": "26/05/2017",
"Ano":"2017",
"Orientadores": [
{
"Orientador":"LEANDRO APARECIDO VILLAS",
"Periodo":"01/03/2015 a 26/05/2017",
"Principal":"Sim"
}
]
},
{
"Aluno": "RAFAEL SOARES PADILHA",
"DataMatricula": "01/03/2009",
"DataSituacao": "01/09/2017",
"Ano":"2017",
"Orientadores": [
{
"Orientador":"JACQUES WAINER",
"Periodo":"27/07/2015 a 01/09/2017",
"Principal":"Sim"
},
{
"Orientador":"FERNANDA ALCANTARA ANDALO",
"Periodo":"01/09/2015 a 01/09/2017",
"Principal":"N�o"
}
]
},
{
"Aluno": "STALLIN ESTEFFERSON FERREIRA DA SILVA",
"DataMatricula": "22/02/2013",
"DataSituacao": "26/04/2017",
"Ano":"2017",
"Orientadores": [
{
"Orientador":"MARIO LUCIO CORTES",
"Periodo":"24/06/2013 a 26/04/2017",
"Principal":"Sim"
}
]
},
{
"Aluno": "THIAGO FRANCO DE MORAES",
"DataMatricula": "10/08/2017",
"DataSituacao": "17/11/2017",
"Ano":"2017",
"Orientadores": [
{
"Orientador":"HELIO PEDRINI",
"Periodo":"10/08/2017 a 17/11/2017",
"Principal":"Sim"
}
]
},
{
"Aluno": "VANDALIS GIANSANTE",
"DataMatricula": "01/09/2014",
"DataSituacao": "31/08/2017",
"Ano":"2017",
"Orientadores": [
{
"Orientador":"RICARDO DA SILVA TORRES",
"Periodo":"01/09/2014 a 31/08/2017",
"Principal":"Sim"
}
]
},
{
"Aluno": "THIAGO AUGUSTO LOPES GENEZ",
"DataMatricula": "01/03/2010",
"DataSituacao": "13/12/2017",
"Ano":"2017",
"Orientadores": [
{
"Orientador":"EDMUNDO ROBERTO MAURO MADEIRA",
"Periodo":"01/03/2010 a 13/12/2017",
"Principal":"Sim"
},
{
"Orientador":"LUIZ FERNANDO BITTENCOURT",
"Periodo":"01/03/2010 a 13/12/2017",
"Principal":"N�o"
}
]
},
{
"Aluno": "THIAGO FERNANDES CREPALDI",
"DataMatricula": "01/08/2012",
"DataSituacao": "19/12/2017",
"Ano":"2017",
"Orientadores": [
{
"Orientador":"EDUARDO CANDIDO XAVIER",
"Periodo":"01/08/2012 a 19/12/2017",
"Principal":"N�o"
},
{
"Orientador":"NELSON LUIS SALDANHA DA FONSECA",
"Periodo":"01/08/2012 a 19/12/2017",
"Principal":"Sim"
}
]
},
{
"Aluno": "TIAGO REZENDE CAMPOS FALCAO",
"DataMatricula": "04/08/2009",
"DataSituacao": "03/02/2017",
"Ano":"2017",
"Orientadores": [
{
"Orientador":"RODOLFO JARDIM DE AZEVEDO",
"Periodo":"04/08/2009 a 03/02/2017",
"Principal":"Sim"
}
]
}