# -*- coding: utf-8 -*-
import scrapy
import re

class SucupiraSpider(scrapy.Spider):
    name = 'Parameters'
    allowed_domains = ['sucupira.capes.gov.br']
    start_urls = [
        'https://sucupira.capes.gov.br//sucupira/public/consultas/coleta/envioColeta/dadosFotoEnvioColeta.jsf'
    ]

    def parse(self, response):
        print('passei')
        yield scrapy.Request('https://sucupira.capes.gov.br//sucupira/public/consultas/coleta/envioColeta/dadosFotoEnvioColeta.jsf',
                             callback=self.parse_capes)

    def parse_capes(self, response):
        # JSESSIONID
        jsessionid = str(response.headers.get('Set-Cookie'))
        pos1 = jsessionid.find('JSESSIONID=')
        pos2 = jsessionid.find('; Path=/')
        jsessionid = jsessionid[pos1 + 11:pos2]
        # JSESSIONID FIM

        # VIEWSTATE
        bodystr = str(response.body)
        pos1 = bodystr.find('id=\"javax.faces.ViewState" value=')
        pos2 = bodystr.find('\" autocomplete="off" />')
        viewstate = bodystr[pos1 + 34 : pos2]
        # VIEWSTATE FIM


        # ID que vem depois do form ex: j_idt31
        pos1 = bodystr.find('form:j_idt')
        pos2 = bodystr.find(':calendarioid')
        iD = bodystr[pos1 +  10: pos2]
        # ID FIM 

        file1 = open("params.txt", "w")
        file1.write(jsessionid + '//' + viewstate + '//' + iD + '//\n')
        #file1.write(viewstate + '\n')
        #file1.write(iD + '\n')
        file1.close()

        #Pegar o numero prod. Intelectuais
    #     self.start_urls.append('https://sucupira.capes.gov.br/sucupira/public/consultas/coleta/envioColeta/dadosFotoEnvioColeta.jsf;jsessionid=' + jsessionid)
    #     yield scrapy.FormRequest(
    #         'https://sucupira.capes.gov.br/sucupira/public/consultas/coleta/envioColeta/dadosFotoEnvioColeta.jsf;jsessionid=' + jsessionid,
    #         method='POST',
    #         headers={
    #             'Content-type': 'application/x-www-form-urlencoded;charset=UTF-8',
    #             'Faces-Request': 'partial/ajax',
    #             'Referer': 'https://sucupira.capes.gov.br//sucupira/public/consultas/coleta/envioColeta/dadosFotoEnvioColeta.jsf',
    #             'Sec-Fetch-Mode': 'cors'
    #         },
    #         formdata={
    #             'form': 'form',
    #             'form:j_idt' + iD + ':calendarioid': '579',
    #             'form:j_idt' + iD + ':inst:valueId': '3603',
    #             'form:j_idt' + iD + ':inst:input': '32008015 PONTIFÍCIA UNIVERSIDADE CATÓLICA DE MINAS GERAIS (PUC/MG)',
    #             'form:j_idt' + iD + ':j_idt392': '52230',
    #             'javax.faces.ViewState': viewstate,
    #             'javax.faces.source': 'form:consultar',
    #             'javax.faces.partial.event': 'click',
    #             'javax.faces.partial.execute': 'form:consultar @component',
    #             'javax.faces.partial.render': '@component',
    #             'javax.faces.behavior.event': 'action',
    #             'org.richfaces.ajax.component': 'form:consultar',
    #             'AJAX:EVENTS_COUNT': '1',
    #             'javax.faces.partial.ajax': 'true'
    #         },
    #         callback=self.numberInfo,
    #         )

    # def numberInfo(self, response):
    #     print(response.body)





