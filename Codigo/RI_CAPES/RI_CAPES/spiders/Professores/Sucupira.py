# -*- coding: utf-8 -*-
import scrapy
import re
import sys

class SucupiraSpider(scrapy.Spider):
    name = 'Sucupira'
    allowed_domains = ['sucupira.capes.gov.br']

    file0 = open("params.txt", "r")
    parametros = file0.readline()
    dados = re.search('(.*)//(.*)//(.*)//', parametros)
    jsessionid = str(dados.group(1))
    viewstate = str(dados.group(2))
    iD = str(dados.group(3))

    parametros = file0.readline()
    print('parametros ' + parametros)
    dados = re.search('(.*)//(.*)//(.*)//(.*)//(.*)', parametros)
    IDInstituicao = str(dados.group(1))
    NomeInstituicao = str(dados.group(2))
    IDCurso = str(dados.group(3))
    IDCalendario = str(dados.group(5))

    start_urls = [
        'https://sucupira.capes.gov.br/sucupira/public/consultas/coleta/envioColeta/dadosFotoEnvioColeta.jsf;jsessionid=' + str(jsessionid),
    ]
    def parse(self, response):
        yield scrapy.FormRequest(
            'https://sucupira.capes.gov.br/sucupira/public/consultas/coleta/envioColeta/dadosFotoEnvioColeta.jsf;jsessionid=' + str(self.jsessionid),
            method='POST',
            formdata={
                'form': 'form',
                'form:j_idt30:calendarioid': str(self.IDCalendario),
                'form:j_idt30:inst:valueId': str(self.IDInstituicao),
                'form:j_idt30:inst:input': str(self.NomeInstituicao),
                'form:j_idt30:j_idt391': str(self.IDCurso),
                'form: j_idt92:j_idt252:j_idt1571:cmbPagina': '1',
                'javax.faces.ViewState': str(self.viewstate),
                'javax.faces.source': 'form:j_idt92:j_idt252:j_idt254',
                'javax.faces.partial.event': 'click',
                'javax.faces.partial.execute': 'form:j_idt92:j_idt252:j_idt254@component',
                'javax.faces.partial.render': '@component',
                'javax.faces.behavior.event': 'action',
                'org.richfaces.ajax.component': 'form:j_idt92:j_idt252:j_idt254',
                'AJAX:EVENTS_COUNT': '1',
                'javax.faces.partial.ajax': 'true'
            },
            headers={
                'Content-type': 'application/x-www-form-urlencoded;charset=UTF-8',
                'Faces-Request': 'partial/ajax',
                'Referer': 'https://sucupira.capes.gov.br/sucupira/public/consultas/coleta/envioColeta/dadosFotoEnvioColeta.jsf',
                'Sec-Fetch-Mode': 'cors'
            },
            callback=self.parse_producoes_intelectuais_prof,
        )
    def parse_producoes_intelectuais_prof(self, response):
        #print(str(response.body))
        texto = str(response.body)

        i = 1

        #  A funcao split divide/separa todos professores.
        texto = texto.split('<td>')
        print('textoAAA ', texto[i])
        file_periodico = open("professores.txt", "a")
        file_periodico.write('[\n{\n\"title\": PUC-MG,\n\"teachers\": [\n')

        while (i < len(texto)):
            # A cada 4 partes temos: 1a Parte - Titulo     2a Parte - Autor    3a Parte - Tipo de publicação       4a Parte - ano
            #  A 3a parte  é onde está o tipo da publicacao
            professor = texto[i]
            # Copia até o proximo </td>
            professor = professor[0:professor.index('</td>')]
            print('professor ', professor)

            tipo = texto[i + 1]
            tipo = tipo[0:tipo.index('\\n')]
            # Passa para o proximo professor
            i = i + 2

            if (str(self.IDCalendario) == '579'):
                Ano = '2018'
            else:
                Ano = '2017'

            imprime = '{\n\"Nome\": ' + '\"' + professor + '\"' + ',\n\"Tipo\": '  '\"' + tipo +'\",\n\"Ano\":\"' + Ano +'\"\n}'
            if i < len(texto):
                imprime = imprime + ','
            file_periodico.write(imprime + '\n')

        file_periodico.write(']\n}\n]')
        file_periodico.close()