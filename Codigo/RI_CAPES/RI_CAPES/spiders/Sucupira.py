# -*- coding: utf-8 -*-
import scrapy
import re
import sys

def AcertaTexto(texto):
    novo_texto = texto

    novo_texto = novo_texto.replace('&Ccedil;', 'Ç')
    novo_texto = novo_texto.replace('&ccedil;', 'ç')

    novo_texto = novo_texto.replace('&Atilde;', 'Ã')
    novo_texto = novo_texto.replace('&atilde;', 'ã')
    novo_texto = novo_texto.replace('&Otilde;', 'Õ')
    novo_texto = novo_texto.replace('&otilde;', 'õ')

    novo_texto = novo_texto.replace('&Aacute;', 'Á')
    novo_texto = novo_texto.replace('&aacute;', 'á')
    novo_texto = novo_texto.replace('&Eacute;', 'É')
    novo_texto = novo_texto.replace('&eacute;', 'é')
    novo_texto = novo_texto.replace('&Iacute;', 'Í')
    novo_texto = novo_texto.replace('&iacute;', 'í')
    novo_texto = novo_texto.replace('&Oacute;', 'Ó')
    novo_texto = novo_texto.replace('&oacute;', 'ó')
    novo_texto = novo_texto.replace('&Uacute;', 'Ú')
    novo_texto = novo_texto.replace('&uacute;', 'ú')

    novo_texto = novo_texto.replace('&Acirc;', 'Â')
    novo_texto = novo_texto.replace('&acirc;', 'â')
    novo_texto = novo_texto.replace('&Ecirc;', 'Ê')
    novo_texto = novo_texto.replace('&ecirc;', 'ê')

    novo_texto = novo_texto.replace('&#8217;', '\'')
    novo_texto = novo_texto.replace('&#8209;', '-')

    novo_texto = novo_texto.replace('&Agrave;', 'À')
    novo_texto = novo_texto.replace('&agrave;', 'à')

    return novo_texto

def VerificaStopWord(termo):
    if termo in ('UM', 'A', 'AS', 'E', 'O', 'OS', 'DE', 'PARA', 'COM', 'SEM', 'EM', 'DA', 'DO', 'NÃO', 'AN', 'FROM', 'THE', 'AND', 'OR', 'TO', 'WITH', 'WITHOUT', 'ON', 'OF', 'FOR', 'IN', '&'):
        result = True
    else:
        result = False

def ComparaNomeConferencia(nome_conf, nome_qualis):
    nome_conf = nome_conf.split(' ')

    i = 1
    while(i < len(nome_conf)):
        if not VerificaStopWord(nome_conf[i]):
            print(nome_conf[i])
        i = i + 1

def ObtemQualis():
    # Obtendo o Qualis da conferência
    arqqualis = open('QualisConferencias.txt', 'r')
    # print('Qualis: ', textoqualis)

    i = 1
    while (i <= len(textoqualis)):
        textoqualis = arqqualis.readline()
        i = i + 1

    arqqualis.close()

    teste = ComparaNomeConferencia(nome_artigo, nome_artigo)

    # result = re.search(str(issn) + '(.*)Qualis (.*)\n', textoqualis)
    # Para deixar somente a sigla
    sigla = sigla.replace('2019', '')
    sigla = sigla.replace('2018', '')
    sigla = sigla.replace('2017', '')
    sigla = sigla.replace('18', '')
    sigla = sigla.replace('17', '')
    sigla = sigla.replace('\\', '')
    sigla = sigla.replace('\'', '')
    sigla = sigla.replace(' ', '')

    if sigla != 'undefined':
        result = re.search(sigla.upper() + '(.*)\n', textoqualis)
        if result:
            nivel_qualis = result.group(1)[len(result.group(1)) - 2:len(result.group(1))]
        else:
            nivel_qualis = 'undefined'
        # print('Nivel:', nivel_qualis)
    else:
        result = re.search('(.*)' + evento + '\t(.*)\n', textoqualis)
        if result:
            nivel_qualis = str(result.group(2))
        else:
            nivel_qualis = 'undefined'

    result = nivel_qualis


class SucupiraSpider(scrapy.Spider):
    name = 'Sucupira'
    allowed_domains = ['sucupira.capes.gov.br']
    linha = ''
    def __init__(self, paginacao='', *args, **kwargs):
        super(SucupiraSpider, self).__init__(*args, **kwargs)
        self.linha = paginacao

    file0 = open("params.txt", "r")
    parametros = file0.readline()
    dados = re.search('(.*)//(.*)//(.*)//', parametros)
    jsessionid = str(dados.group(1))
    viewstate = str(dados.group(2))
    iD = str(dados.group(3))
    print('sessao ', str(jsessionid) + 'state' + str(viewstate))

    #for x in range(3, paginacao):
        #file0.readline()

    parametros = file0.readline()
    print('parametros ' + parametros)
    dados = re.search('(.*)//(.*)//(.*)//(.*)//(.*)', parametros)
    IDInstituicao = str(dados.group(1))
    NomeInstituicao = str(dados.group(2))
    IDCurso = str(dados.group(3))
    pagina = str(dados.group(4))
    IDCalendario = str(dados.group(5))

    file0.close()

    start_urls = [
        'https://sucupira.capes.gov.br/sucupira/public/consultas/coleta/envioColeta/dadosFotoEnvioColeta.jsf;jsessionid=' + str(jsessionid),
    ]

    def parse(self, response):
        print('Instituicao ', str(self.IDInstituicao) + 'nome ' + str(self.NomeInstituicao) + ' curso ' + str(self.IDCurso) + ' pagina ' + str(self.pagina))

        if str(self.pagina) == '1':
            yield scrapy.FormRequest(
                'https://sucupira.capes.gov.br/sucupira/public/consultas/coleta/envioColeta/dadosFotoEnvioColeta.jsf;jsessionid=' + str(self.jsessionid),
                #'https://sucupira.capes.gov.br/sucupira/public/consultas/coleta/envioColeta/dadosFotoEnvioColeta.jsf;jsessionid=MBkoU-HFt+q91xf3G1olPNHk.sucupira-218',
                method='POST',
                formdata={
                    'form': 'form',
                    'form:j_idt30:calendarioid': str(self.IDCalendario),
                    'form:j_idt30:inst:valueId': str(self.IDInstituicao),
                    'form:j_idt30:inst:input': str(self.NomeInstituicao),
                    'form:j_idt30:j_idt391': str(self.IDCurso),
                    'javax.faces.ViewState': str(self.viewstate),
                    'javax.faces.source': 'form:j_idt92:j_idt312:j_idt314',
                    'javax.faces.partial.event': 'click',
                    'javax.faces.partial.execute': 'form:j_idt92:j_idt312:j_idt314 @component',
                    'javax.faces.partial.render': '@component',
                    'javax.faces.behavior.event': 'action',
                    'org.richfaces.ajax.component': 'form:j_idt92:j_idt312:j_idt314',
                    'AJAX:EVENTS_COUNT': '1',
                    'javax.faces.partial.ajax': 'true'
                },
                headers={
                    'Content-type': 'application/x-www-form-urlencoded;charset=UTF-8',
                    'Faces-Request': 'partial/ajax',
                    'Referer': 'https://sucupira.capes.gov.br//sucupira/public/consultas/coleta/envioColeta/dadosFotoEnvioColeta.jsf',
                    'Sec-Fetch-Mode': 'cors'
                },
                meta={'dont_merge_cookies': True},
                callback=self.parse_producoes_intelectuais_exp,
            )
        else:
            self.pagina = int(self.pagina) - 1
            yield scrapy.FormRequest(
                'https://sucupira.capes.gov.br/sucupira/public/consultas/coleta/envioColeta/dadosFotoEnvioColeta.jsf;jsessionid=' + str(self.jsessionid),
                method='POST',
                formdata={
                    'form': 'form',
                    'form:j_idt30:calendarioid': str(self.IDCalendario),
                    'form:j_idt30:inst:valueId': str(self.IDInstituicao),
                    'form:j_idt30:inst:input': str(self.NomeInstituicao),
                    'form:j_idt30:j_idt391': str(self.IDCurso),
                    'form:j_idt92:j_idt312:j_idt1750:cmbPagina': str(self.pagina),
                    'form:j_idt92:j_idt312:j_idt2656:cmbPagina': '1',
                    'javax.faces.ViewState': str(self.viewstate),
                    'javax.faces.source: form:j_idt92:j_idt312:j_idt1750':'btProximo',
                    'javax.faces.partial.event': 'click',
                    'javax.faces.partial.execute':'form:j_idt92:j_idt312:j_idt1750:btProximo@component',
                    'javax.faces.partial.render': '@component',
                    'org.richfaces.ajax.component':'form:j_idt92:j_idt312:j_idt1750:btProximo',
                    'form:j_idt92:j_idt312:j_idt1750:btProximo':'form:j_idt92:j_idt312:j_idt1750:btProximo',
                    'AJAX:EVENTS_COUNT': '1',
                    'javax.faces.partial.ajax': 'true'
                },
                headers={
                    'Content-type': 'application/x-www-form-urlencoded;charset=UTF-8',
                    'Faces-Request': 'partial/ajax',
                    'Referer': 'https://sucupira.capes.gov.br//sucupira/public/consultas/coleta/envioColeta/dadosFotoEnvioColeta.jsf',
                    'Sec-Fetch-Mode': 'cors'
                },
                meta={'dont_merge_cookies': True},
                callback=self.parse_producoes_intelectuais_exp,
            )

    def parse_producoes_intelectuais_exp(self, response):
        #response = response.decode("utf-8")
        texto = str(response.body)

        i = 1

        #  A funcao split divide/separa todos artigos.
        texto = texto.split('<td>')

        while(i < len(texto)):
            #A cada 4 partes temos: 1a Parte - Titulo     2a Parte - Autor    3a Parte - Tipo de publicação       4a Parte - ano
            #  A 3a parte  é onde está o tipo da publicacao
            tipo = texto[i+2]
            # Copia até o proximo </td>
            tipo = tipo[0:tipo.index('</td>')]

            # na 4a parte está o idProducao  que corresponde ao ID de cada artigo
            idProducao = texto[i+3]
            #Encontrando o idProducao
            posIDProducao = idProducao.find('idProducao')
            #Copiando só a parte do IdProducao
            idProducao = idProducao[posIDProducao+11:posIDProducao+19]

            #publicacao = tipo + '\nidProducao ' +  idProducao + '\n\n'

            #Passa para o proximo artigo
            i = i + 4

            if tipo == 'TRABALHO EM ANAIS':
                urlPagina = 'https://sucupira.capes.gov.br/sucupira/public/consultas/coleta/envioColeta/detalhesDados/viewProducao.jsf?popup=true&idProducao=' + idProducao
                yield response.follow(urlPagina, self.parse_conferencia)
            elif tipo == 'ARTIGO EM PERI&Oacute;DICO':
                urlPagina = 'https://sucupira.capes.gov.br/sucupira/public/consultas/coleta/envioColeta/detalhesDados/viewProducao.jsf?popup=true&idProducao=' + idProducao
                yield response.follow(urlPagina, self.parse_periodicos)
            else:
                print('Outros: ', tipo)

    def parse_periodicos(self, response):
        texto = str(response.body)

        # Extraindo o nome do do artigo
        pos0 = texto.find('Ano')
        ano = texto[pos0 + 109:pos0 + 113]

        pos1 = texto.find('T&iacute;tulo:')
        pos2 = texto.find('Autores')
        nome_artigo = texto[pos1 + 91:pos2 - 125]

        # Para encontrar os autores
        posTipo = texto.find('Tipo:')
        autores = texto[pos2:posTipo]
        #  A funcao split divide/separa todos autores.
        autores = autores.split('<td>')

        # Extraindo o nome do evento
        pos3 = texto.find('ISSN')
        pos4 = texto.find('Nome da editora:')
        evento = texto[pos3 + 146:pos4 - 382]
        issn = texto[pos3 + 146:pos3 + 155]

        #Obtendo o Qualis do periodico
        arqqualis = open('QualisPeriodicos.txt', 'r')
        textoqualis = arqqualis.read()
        arqqualis.close()

        #result = re.search(str(issn) + '(.*)Qualis (.*)\n', textoqualis)
        result = re.search(str(issn) + '(.*)\n(.*)\n(.*)\n', textoqualis)

        if result:
            nivel_qualis = str(result.group(3))
        else:
            nivel_qualis = 'undefined'

        #imprime = 'Artigo: ' + nome_artigo + '\nEvento: ' + evento + '\nISSN: ' + issn + '\nNível:' + str(nivel_qualis)

        imprime = '{\n\"Artigo\": ' + '\"' + nome_artigo + '\"' + ',\n\"Evento\": '  '\"' + evento + '\"'',\n\"ISSN\": ' + '\"' + issn + '\"' + ',\n\"Nivel\":' + '\"' + nivel_qualis + '\"'+ ',\n\"Ano\":' + '\"' + str(ano) + '\",'
        imprime = imprime + '\n\"Autores\": ['

        #Para incluir os autores
        i = 1
        while (i < len(autores)):
            #    #A cada 3 partes temos: 1a Parte - Ordem     2a Parte - Nome do Autor    3a Parte - Categoria
            ordem = autores[i]
            # Copia até o proximo </td>
            ordem = ordem[0:ordem.index('</td>')]

            nome_autor = autores[i + 1]
            # Copia até o proximo </td>
            nome_autor = nome_autor[0:nome_autor.index('</td>')]

            categoria = autores[i + 2]
            # Copia até o proximo </td>
            categoria = categoria[0:categoria.index('</td>')]
            imprime = imprime + '\n{\n\"Ordem\":\"' + ordem + '\",\n\"Autor\":\"' + nome_autor + '\",\n\"Categoria\":\"' + categoria + '\"\n}'

            i = i + 3
            if i < len(autores):
                imprime = imprime + ','
        #imprime = imprime + '\"Ordem\":' + autores
        imprime = imprime + '\n]\n},\n'

        imprime = AcertaTexto(imprime)
        file_periodico = open("periodicos.txt", "a")
        file_periodico.write(imprime)
        file_periodico.close()
    #FIM PARSE_PERIODICO

    def parse_conferencia(self, response):
        texto = str(response.body)

        # Extraindo o nome do do artigo
        posAno = texto.find('Ano')
        ano = texto[posAno + 109:posAno + 113]

        #Extraindo o nome do evento
        pos = texto.find('Nome do evento')
        pos2 = texto.find('ISBN')
        evento = texto[pos + 120:pos2 - 382]
        #Verifica se foi colocada a sigla do evento
        if evento.find('(') > -1:
            sigla = evento[evento.find('(')+1:evento.find(')')]
        else:
            if evento.find('-') > -1:
                sigla = evento[evento.find('-')+1:len(evento)]
                print('SIGLA', sigla, ' posicao ', str(len(evento)), '      ', str(evento.find('-')+1))
            else:
                sigla = 'undefined'

        # Extraindo o nome do do artigo
        pos3 = texto.find('T&iacute;tulo:')
        pos4 = texto.find('Autores')
        nome_artigo = texto[pos3 + 91:pos4 - 125]

        # Para encontrar os autores
        posTipo = texto.find('Tipo:')
        autores = texto[pos4:posTipo]
        #  A funcao split divide/separa todos autores.
        autores = autores.split('<td>')

        nivel_qualis = ObtemQualis()

        #imprime = 'Artigo: ' + nome_artigo + '\nEvento: ' + evento + '\nSigla:' + sigla + '\nAno:' + ano + 'Nivel:' + nivel_qualis
        imprime = '{\n\"Artigo\": ' + '\"' + nome_artigo + '\"' + ',\n\"Evento\": '  '\"' + evento + '\"'',\n\"Sigla\": ' + '\"' + sigla + '\"' + ',\n\"Nivel\":' + '\"' + nivel_qualis + '\"' + ',\n\"Ano\":' + '\"' + str(ano) + '\",'
        imprime = imprime + '\n\"Autores\": ['

        # Para incluir os autores
        i = 1
        while (i < len(autores)):
            #    #A cada 3 partes temos: 1a Parte - Ordem     2a Parte - Nome do Autor    3a Parte - Categoria
            ordem = autores[i]
            # Copia até o proximo </td>
            ordem = ordem[0:ordem.index('</td>')]

            nome_autor = autores[i + 1]
            # Copia até o proximo </td>
            nome_autor = nome_autor[0:nome_autor.index('</td>')]

            categoria = autores[i + 2]
            # Copia até o proximo </td>
            categoria = categoria[0:categoria.index('</td>')]
            imprime = imprime + '\n{\n\"Ordem\":\"' + ordem + '\",\n\"Autor\":\"' + nome_autor + '\",\n\"Categoria\":\"' + categoria + '\"\n}'

            i = i + 3
            if i < len(autores):
                imprime = imprime + ','
        # imprime = imprime + '\"Ordem\":' + autores
        imprime = imprime + '\n]\n},\n'

        imprime = AcertaTexto(imprime)
        file4 = open("conferencias.txt", "a")
        file4.write(imprime)
        file4.close()
