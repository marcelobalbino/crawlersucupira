# -*- coding: utf-8 -*-
import scrapy
import re
import sys

def AcertaTexto(texto):
    novo_texto = texto

    novo_texto = novo_texto.replace('&Ccedil;', 'Ç')
    novo_texto = novo_texto.replace('&ccedil;', 'ç')

    novo_texto = novo_texto.replace('&Atilde;', 'Ã')
    novo_texto = novo_texto.replace('&atilde;', 'ã')
    novo_texto = novo_texto.replace('&Otilde;', 'Õ')
    novo_texto = novo_texto.replace('&otilde;', 'õ')

    novo_texto = novo_texto.replace('&Aacute;', 'Á')
    novo_texto = novo_texto.replace('&aacute;', 'á')
    novo_texto = novo_texto.replace('&Eacute;', 'É')
    novo_texto = novo_texto.replace('&eacute;', 'é')
    novo_texto = novo_texto.replace('&Iacute;', 'Í')
    novo_texto = novo_texto.replace('&iacute;', 'í')
    novo_texto = novo_texto.replace('&Oacute;', 'Ó')
    novo_texto = novo_texto.replace('&oacute;', 'ó')
    novo_texto = novo_texto.replace('&Uacute;', 'Ú')
    novo_texto = novo_texto.replace('&uacute;', 'ú')

    novo_texto = novo_texto.replace('&Acirc;', 'Â')
    novo_texto = novo_texto.replace('&acirc;', 'â')
    novo_texto = novo_texto.replace('&Ecirc;', 'Ê')
    novo_texto = novo_texto.replace('&ecirc;', 'ê')

    novo_texto = novo_texto.replace('&#8217;', '\'')

    novo_texto = novo_texto.replace('&Agrave;', 'À')
    novo_texto = novo_texto.replace('&agrave;', 'à')

    return novo_texto

class SucupiraSpider(scrapy.Spider):
    name = 'Sucupira'
    allowed_domains = ['sucupira.capes.gov.br']

    file0 = open("params.txt", "r")
    parametros = file0.readline()
    dados = re.search('(.*)//(.*)//(.*)//', parametros)
    jsessionid = str(dados.group(1))
    viewstate = str(dados.group(2))
    iD = str(dados.group(3))

    parametros = file0.readline()
    print('parametros ' + parametros)
    dados = re.search('(.*)//(.*)//(.*)//(.*)//(.*)', parametros)
    IDInstituicao = str(dados.group(1))
    NomeInstituicao = str(dados.group(2))
    IDCurso = str(dados.group(3))
    IDCalendario = str(dados.group(5))

    start_urls = [
        'https://sucupira.capes.gov.br/sucupira/public/consultas/coleta/envioColeta/dadosFotoEnvioColeta.jsf;jsessionid=' + str(jsessionid),
    ]
    def parse(self, response):
        yield scrapy.FormRequest(
            'https://sucupira.capes.gov.br/sucupira/public/consultas/coleta/envioColeta/dadosFotoEnvioColeta.jsf;jsessionid=' + str(self.jsessionid),
            method='POST',
            formdata={
                'form': 'form',
                'form:j_idt30:calendarioid': str(self.IDCalendario),
                'form:j_idt30:inst:valueId': str(self.IDInstituicao),
                'form:j_idt30:inst:input': str(self.NomeInstituicao),
                'form:j_idt30:j_idt391': str(self.IDCurso),
                #'form: j_idt92:j_idt300:j_idt1423':'cmbPagina: 1',
                'javax.faces.ViewState': str(self.viewstate),
                'javax.faces.source': 'form:j_idt92:j_idt300:j_idt302',
                'javax.faces.partial.event': 'click',
                'javax.faces.partial.execute': 'form:j_idt92:j_idt300:j_idt302 @ component',
                'javax.faces.partial.render': '@component',
                'javax.faces.behavior.event': 'action',
                'org.richfaces.ajax.component': 'form:j_idt92:j_idt300:j_idt302',
                'AJAX:EVENTS_COUNT': '1',
                'javax.faces.partial.ajax': 'true'
            },
            headers={
                'Content-type': 'application/x-www-form-urlencoded;charset=UTF-8',
                'Faces-Request': 'partial/ajax',
                'Referer': 'https://sucupira.capes.gov.br/sucupira/public/consultas/coleta/envioColeta/dadosFotoEnvioColeta.jsf',
                'Sec-Fetch-Mode': 'cors'
            },
            callback=self.parse_producoes_intelectuais_prof,
        )
    def parse_producoes_intelectuais_prof(self, response):
        texto = str(response.body)

        i = 1
        #  A funcao split divide/separa todos professores.
        texto = texto.split('<td>')

        while(i < len(texto)):
        #A cada 4 partes temos: 1a Parte - Título     2a Parte - Autor    3a Parte - Tipo de producao   4aParte - Data de defesa
            titulo = texto[i]
            #aluno = texto[i+1]
            #tipo_producao = texto[i+2]
            dt_defesa = texto[i+3]
            # na 3a parte está o idDiscente  que corresponde ao ID de cada discente para abrir a pagina com detalhes do mesmo
            posIDTrabalho = dt_defesa.find('idTrabalho=')

            idTrabalho = dt_defesa[posIDTrabalho+11:posIDTrabalho+19]

            # Passa para o proximo aluno
            i = i + 4

            print('artigo ',titulo + 'id '+ idTrabalho)
            urlPagina = 'https://sucupira.capes.gov.br/sucupira/public/consultas/coleta/envioColeta/detalhesDados/viewTrabalhoConclusao.jsf?popup=true&idTrabalho=' + idTrabalho
            yield response.follow(urlPagina, self.parse_trabalhos)

    def parse_trabalhos(self, response):
        texto = str(response.body)
        #print(texto)

        # Extraindo o titulo
        posTitulo = texto.find('T&iacute;tulo:')
        posAutor = texto.find('Autor:')
        titulo = texto[posTitulo:posAutor]
        titulo = titulo[titulo.index('<div class="col-md-10">')+23: titulo.index('<div class="row">')-68]

        #Aluno
        posTipo = texto.find('Tipo de Trabalho de Conclus&atilde;o:')
        aluno = texto[posAutor:posTipo]
        aluno = aluno[aluno.index('<div class="col-md-10">') + 23: aluno.index('<div class="row">') - 68]

        #Tipo de Trabalho
        posAbr = texto.find('Abreviatura:')
        TipoTrab = texto[posTipo:posAbr]
        TipoTrab = TipoTrab[TipoTrab.index('<div class="col-md-10">') + 23: TipoTrab.index('<div class="row">') - 68]

        #Idioma
        posIdioma = texto.find('Idioma:')
        posBib = texto.find('Biblioteca depositada:')
        Idioma = texto[posIdioma:posBib]
        Idioma = Idioma[Idioma.index('<div class="col-md-10">') + 23: Idioma.index('<div class="row">') - 68]

        if (str(self.IDCalendario) == '579'):
            Ano = '2018'
        else:
            Ano = '2017'

        imprime = '{\n\"Titulo\":' + '\"' + titulo + '\",\n\"Aluno\":\"' + aluno + '\",\n\"TipoTrabalho\":' + '\"' + TipoTrab + '\",\n\"Idioma\":' + '\"' + Idioma +'\",\n\"Ano\":\"' + Ano +'\"\n},\n'
        imprime = AcertaTexto(imprime)

        file_trabalhos = open("trabalhos.txt", "a")
        file_trabalhos.write(imprime)
        file_trabalhos.close()